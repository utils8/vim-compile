```
podman build -t vim-installer -f https://gitlab.com/utils8/vim-compile/-/raw/master/Dockerfile.vim .
podman run --rm -v $HOME/.local:/opt/vim -w /vim <name>

# Then link the vim path to /opt/vim
```
