FROM docker.io/gcc:11

RUN apt update && \
    apt install python3-dev -y

RUN git clone -b v9.0.0654 --depth=1 https://github.com/vim/vim.git

RUN cd vim && \
    ./configure --prefix=/opt/vim \
    --with-features=huge \
    --enable-python3interp=dynamic \
    --enable-cscope \
    --enable-gui=no \
    --enable-fail-if-missing

RUN cd vim && make -j5

WORKDIR /vim
CMD ["make", "install"]
